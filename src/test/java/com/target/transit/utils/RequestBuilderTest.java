package com.target.transit.utils;

import com.target.transit.request.ServiceRequest;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class RequestBuilderTest {

    @Test
    public void shouldBuildServiceRequest() {
        //When
        ServiceRequest serviceRequest = RequestBuilder.buildRequest("5", 1, "Target");

        //Then
        assertThat(serviceRequest.route, is("5"));
        assertThat(serviceRequest.direction, is(1));
        assertThat(serviceRequest.stop, is("Target"));
    }
}
