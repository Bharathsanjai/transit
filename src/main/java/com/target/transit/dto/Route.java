package com.target.transit.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class Route implements Serializable {

    @JsonProperty("Description")
    public String description;
    @JsonProperty("ProviderID")
    public String providerID;
    @JsonProperty("Route")
    public String route;

}
