package com.target.transit.utils;

import com.target.transit.request.ServiceRequest;
import org.springframework.web.reactive.function.server.ServerRequest;

import java.util.Optional;

public class RequestBuilder {

    public static ServiceRequest buildRequest(String route, int direction, String stop) {
        return new ServiceRequest(route, direction, stop);
    }

    public static ServiceRequest buildRequestFor(ServerRequest serverRequest) {
        ServiceRequest serviceRequest = new ServiceRequest();
        Optional<String> route = serverRequest.queryParam("route");
        if(route.isPresent()) {
            serviceRequest.setRoute(route.get());
        }

        Optional<String> direction = serverRequest.queryParam("direction");
        if(direction.isPresent()){
            serviceRequest.setDirection(Integer.valueOf(direction.get()));
        }

        Optional<String> stop = serverRequest.queryParam("stop");
        if(stop.isPresent()) {
            serviceRequest.setStop(stop.get());
        }

        return serviceRequest;
    }

}
