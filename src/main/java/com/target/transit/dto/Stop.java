package com.target.transit.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.io.Serializable;

@Data
@Setter @Getter @AllArgsConstructor @NoArgsConstructor @ToString
public class Stop implements Serializable {

    @JsonProperty("Text")
    public String text;
    @JsonProperty("Value")
    public String value;

}
