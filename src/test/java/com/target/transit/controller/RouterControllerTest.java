package com.target.transit.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.client.WebClient;

@RunWith(SpringJUnit4ClassRunner.class)
@WebFluxTest
public class RouterControllerTest {

    @Autowired
    WebTestClient webTestClient;


    @Test
    public void shouldGetAllRoutes() {
        webTestClient.get().uri("/routes")
                .exchange()
                .expectStatus()
                .isOk();
    }

}
