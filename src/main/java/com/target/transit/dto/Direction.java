package com.target.transit.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.io.Serializable;

@Data
@Setter @Getter @AllArgsConstructor @NoArgsConstructor
public class Direction implements Serializable {

    @JsonProperty("Text")
    public String text;
    @JsonProperty("Value")
    public int value;
}
