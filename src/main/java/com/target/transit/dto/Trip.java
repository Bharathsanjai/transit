package com.target.transit.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Data
@AllArgsConstructor @NoArgsConstructor @Getter @Setter @ToString
public class Trip {

    @JsonProperty("Actual")
    public String actual;

    @JsonProperty("BlockNumber")
    public int blockNumber;

    @JsonProperty("RouteDirection")
    public String routeDirection;

    @JsonProperty("Terminal")
    public String terminal;

    @JsonProperty("DepartureText")
    public String departureText;

    @JsonProperty("DepartureTime")
    public String departureTime;

    @JsonProperty("Description")
    public String description;

    @JsonProperty("Route")
    public String route;


}
