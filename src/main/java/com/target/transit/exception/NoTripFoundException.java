package com.target.transit.exception;

public class NoTripFoundException extends RuntimeException {

    public NoTripFoundException(String message) {
        super(message);
    }
}
