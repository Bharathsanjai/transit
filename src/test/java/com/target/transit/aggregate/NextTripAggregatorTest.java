package com.target.transit.aggregate;

import com.target.transit.client.ServiceConsumer;
import com.target.transit.dto.Route;
import com.target.transit.dto.Stop;
import com.target.transit.dto.Trip;
import com.target.transit.request.ServiceRequest;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class NextTripAggregatorTest {

    @InjectMocks @Spy
    NextTripAggregator nextTripAggregator;
    @Mock
    ServiceConsumer<List<Trip>, ServiceRequest> tripServiceConsumer;
    @Mock
    ServiceConsumer<List<Route>, ServiceRequest> routeServiceConsumer;
    @Mock
    ServiceConsumer<List<Stop>, ServiceRequest> stopServiceConsumer;

    List<Trip> trips = new ArrayList<>();
    List<Route> routes = new ArrayList<>();
    List<Stop> stops = new ArrayList<>();

    ServiceRequest serviceRequest = new ServiceRequest();

    Mono<List<Route>> routeMono = Mono.just(routes);
    Mono<List<Stop>> stopMono = Mono.just(stops);
    Mono<List<Trip>> tripsMono = Mono.just(trips);


    @Before
    public void setUp() {

        initMocks(this);
        nextTripAggregator.initialize();
        createTripList();
        createRouteList();
        createStops();
        serviceRequest.setDirection(4);
        serviceRequest.setRoute("5");
        serviceRequest.setStop("7SOL");
        when(routeServiceConsumer.consume(serviceRequest)).thenReturn(routeMono);
        when(stopServiceConsumer.consume(serviceRequest)).thenReturn(stopMono);
        when(tripServiceConsumer.consume(serviceRequest)).thenReturn(tripsMono);

    }

    public void createTripList() {
        Trip first = new Trip("true", 1139, "NORTHBOUND", "M", "6 Min", "/Date(1567183020000-0500)/", "Fremont Av/Brklyn Ctr/Transit Ctr", "5");
        Trip second = new Trip("true", 1126, "SOUTHBOUND", "F", "15 Min", "/Date(1567183560000-0500)/", "Emerson/26Av-Broadway", "5");
        trips.add(first);
        trips.add(second);
    }

    public void createRouteList() {
        Route first = new Route("METRO Blue Line", "8", "5");
        Route second = new Route("METRO Red Line", "8", "901");
        routes.add(first);
        routes.add(second);
    }

    public void createStops() {
        Stop first = new Stop("7th St  and Olson Memorial Hwy", "7SOL");
        Stop second = new Stop("7th St  and Olson Memorial Hwy", "7SOL");
        stops.add(first);
        stops.add(second);
    }

    @Test
    public void shouldParseDateStringToLong() {
        //When
        long dateString = nextTripAggregator.parseDateString("/Date(1567183020000-0500)/");

        //Then
        assertThat(dateString, is(1567183020000L));
    }

    @Test
    public void shouldCheckTripEligiblity() {
        //When
        boolean tripEligible = nextTripAggregator.isTripEligible("/Date(1567183020000-0500)/");

        //Then
        assertThat(tripEligible, is(false));
    }

    @Test
    public void shouldGetComputeTrip(){

        //When
        Trip computedTrip = nextTripAggregator.getComputedTrip(trips, serviceRequest);

        //Then
        assertThat(computedTrip, is(nullValue()));
    }

    @Test
    public void shouldComputeRoute() {
       //When
       Route route = nextTripAggregator.computeRoute(serviceRequest).block();

       //Then
        assertThat(route, is(routes.get(0)));
    }

    @Test
    public void shouldComputeStop(){
        //When
        Stop stop = nextTripAggregator.computeStop(serviceRequest, routes.get(0)).block();

        //Then
        assertThat(stop, is(stops.get(0)));
    }

    @Test
    public void shouldComputeTrip() {
        //When
        String departureText = nextTripAggregator.computeNextTripTime(serviceRequest).block();

        //Then
        assertThat(departureText, is("No schedule found"));
    }


}
