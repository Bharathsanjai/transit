package com.target.transit.controller;

import com.target.transit.aggregate.NextTripAggregator;
import com.target.transit.client.*;
import com.target.transit.dto.Direction;
import com.target.transit.dto.Route;
import com.target.transit.dto.Stop;
import com.target.transit.dto.Trip;
import com.target.transit.exception.NoTripFoundException;
import com.target.transit.handler.*;
import com.target.transit.request.ServiceRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.server.RequestPredicates;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

import java.util.List;

import static com.target.transit.utils.RequestBuilder.buildRequest;

@RestController
@Slf4j
public class RouteController {

    @Bean
    public RouterFunction<ServerResponse> routeTrips(TripRequestHandler tripRequestHandler){
       return RouterFunctions.route(RequestPredicates.GET("/trips"), tripRequestHandler::handle);
    }

    @Bean
    public RouterFunction<ServerResponse> routeNextSchedule(NextTripRequestHandler nextTripRequestHandler){
        return RouterFunctions.route(RequestPredicates.GET("/departure"), nextTripRequestHandler::handle);
    }

    @Bean
    public RouterFunction<ServerResponse> routeAllRoutes(RoutesHandler routesHandler){
        return RouterFunctions.route(RequestPredicates.GET("/routes"), routesHandler::handle);
    }

    @Bean
    public RouterFunction<ServerResponse> routeAllDirections(DirectionHandler directionHandler){
        return RouterFunctions.route(RequestPredicates.GET("/directions"), directionHandler::handle);
    }

    @Bean
    public RouterFunction<ServerResponse> routeAllStops(StopsHandler stopsHandler){
        return RouterFunctions.route(RequestPredicates.GET("/stops"), stopsHandler::handle);
    }

}
