package com.target.transit.client;

import com.target.transit.dto.Direction;
import com.target.transit.exception.ServiceUnavailableException;
import com.target.transit.request.ServiceRequest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import javax.annotation.PostConstruct;

import java.util.List;

import static java.util.Collections.singletonList;

@Service("DirectionClient")
public class DirectionsClient implements ServiceConsumer<List<Direction>, ServiceRequest> {

    WebClient webClient;

    @PostConstruct
    public void initialise() {
        webClient = WebClient.builder()
                .baseUrl("http://svc.metrotransit.org")
                .defaultHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                .defaultHeader(HttpHeaders.USER_AGENT, "Spring 5 WebClient")
                .build();
    }

    public Mono<List<Direction>> consume(ServiceRequest request) {

        return webClient.get()
                .uri(uriBuilder -> {
                    uriBuilder.path("/NexTrip/DIRECTIONS/" + request.route);
                    MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
                    map.put("format", singletonList("json"));
                    uriBuilder.queryParams(map);
                    return uriBuilder.build();
                })
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError, clientResponse -> {
                    return Mono.error(new ServiceUnavailableException("Service unavailable"));
                })
                .bodyToFlux(Direction.class)
                .collectList();
    }

}
