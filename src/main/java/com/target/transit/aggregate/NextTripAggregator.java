package com.target.transit.aggregate;

import com.target.transit.client.ServiceConsumer;
import com.target.transit.dto.Direction;
import com.target.transit.dto.Route;
import com.target.transit.dto.Stop;
import com.target.transit.dto.Trip;
import com.target.transit.exception.NoTripFoundException;
import com.target.transit.request.ServiceRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import reactor.core.publisher.Mono;

import javax.annotation.PostConstruct;
import java.util.*;

@Component
@Slf4j
public class NextTripAggregator {

    private Map<String, Integer> directions = new HashMap<>();


    @Autowired
    @Qualifier("TripClient")
    ServiceConsumer<List<Trip>, ServiceRequest> tripServiceConsumer;

    @Autowired
    @Qualifier("RouteClient")
    ServiceConsumer<List<Route>, ServiceRequest> routeServiceConsumer;

    @Autowired
    @Qualifier("StopsClient")
    ServiceConsumer<List<Stop>, ServiceRequest> stopServiceConsumer;


    @PostConstruct
    public void initialize() {
        directions.put("SOUTHBOUND", 1);
        directions.put("EASTBOUND", 2);
        directions.put("WESTBOUND", 3);
        directions.put("NORTHBOUND", 4);
    }

    public Mono<String> computeNextTripTime(ServiceRequest serviceRequest) {

        return computeRoute(serviceRequest)
                .flatMap(route -> {
                    return computeStop(serviceRequest, route)
                            .flatMap(stop -> {
                                return computeTrip(serviceRequest, stop)
                                        .flatMap(trip -> {
                                            return Mono.just(trip.departureText);
                                        });
                            });
                })
                .onErrorReturn("No schedule found");
    }

    public long parseDateString(String timeString) {
        String timeInMillis = timeString.substring(timeString.indexOf('(')+1, timeString.indexOf('-'));
        return Long.valueOf(timeInMillis);
    }

    public boolean isTripEligible(String timeString) {

        return parseDateString(timeString) > System.currentTimeMillis();
    }

    public Trip getComputedTrip(List<Trip> trips, ServiceRequest serviceRequest) {

        Trip eligibleTrip = null;

        for(Trip trip: trips) {

            if((serviceRequest.route.equalsIgnoreCase(trip.route) && serviceRequest.direction == directions.get(trip.routeDirection)
                    && isTripEligible(trip.departureTime))) {
                if(eligibleTrip == null) {
                    eligibleTrip = trip;
                } else {
                    eligibleTrip = parseDateString(eligibleTrip.departureTime) > parseDateString(trip.departureTime) ? trip : eligibleTrip;
                }
            }

        }

        return eligibleTrip;
    }


    public Mono<Trip> computeTrip(ServiceRequest serviceRequest, Stop stop) {

        return tripServiceConsumer.consume(serviceRequest)
                .flatMap(trips -> {
                    return Mono.just(getComputedTrip(trips, serviceRequest));
                })
                .onErrorReturn(new Trip());
    }

    public Mono<Stop> computeStop(ServiceRequest serviceRequest, Route route) {

        return stopServiceConsumer.consume(serviceRequest)
                .flatMap(stops -> {

                    return Mono.just(stops.stream().filter(stop -> {

                        if(serviceRequest.stop.equalsIgnoreCase(stop.value))
                            log.debug("Stop Found " +stop);

                        return serviceRequest.stop.equalsIgnoreCase(stop.value);
                    }).findFirst().get());
                })
                .onErrorReturn(new Stop());
    }

    public Mono<Route> computeRoute(ServiceRequest serviceRequest) {

        return routeServiceConsumer.consume(serviceRequest)
                .flatMap(routes -> {

                        return Mono.just(routes.stream().filter(route -> {

                            if(isValidRoute(serviceRequest, route)) {
                                log.debug("route Found " +route);
                            }

                            return isValidRoute(serviceRequest, route);
                        }).findFirst().get());
                })
                .onErrorReturn(new Route());
    }

    public boolean isValidRoute(ServiceRequest serviceRequest, Route route) {

        if(serviceRequest.route.equalsIgnoreCase(route.route))
            return true;

        return false;
    }
}
