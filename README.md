# Transit commute service #

This service provides next available time from the upcoming schedules based on the query inputs

### What is this repository for? ###

* Micro service for fetching the next available schedule time
* Version 1.0

### Frameworks used ###

* Spring webflux
* Reactive Web clients
* Mockito/Junits

### How do I get set up? ###

* Install Java 8
* Install apache maven 3.5.0
* Clone transit repository 
* spin up the transit app

### How do I run app? ###
* mvn clean install
* mvn spring-boot:run

### How do I debug? ###
* mvnDebug spring-boot:run

### Code coverage report ###
* code coverage is calculated by jacoco plugin

### Sample request ###
http://localhost:9393/departure?route=5&direction=4&stop=7SOL

### Sample response ###
2:14
### Available Endpoints ###

## Fetch upcoming schedule for given route, direction and stop##
http://localhost:9393/departure?route=5&direction=4&stop=7SOL

## Fetch all trips for given route, direction and stop##
http://localhost:9393/trips?route=5&direction=4&stop=7SOL

## Fetch all stops for given route and direction##
http://localhost:9393/stops?route=5&direction=4

## Fetch all directions for given route##
http://localhost:9393/directions?route=5

## Fetch all routes##
http://localhost:9393/routes








