package com.target.transit.handler;

import com.target.transit.client.ServiceConsumer;
import com.target.transit.dto.Route;
import com.target.transit.exception.NoTripFoundException;
import com.target.transit.request.ServiceRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.server.HandlerFunction;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

import java.util.List;

import static com.target.transit.utils.RequestBuilder.buildRequestFor;

@Component
@Slf4j
public class RoutesHandler implements HandlerFunction {

    @Autowired
    @Qualifier("RouteClient")
    ServiceConsumer<List<Route>, ServiceRequest> routeServiceConsumer;

    @Override
    public Mono handle(ServerRequest serverRequest) {
        return routeServiceConsumer.consume(buildRequestFor(serverRequest))
                .flatMap(trips -> ServerResponse.ok().body(BodyInserters.fromObject(trips)))
                .onErrorResume(NoTripFoundException.class, e -> ServerResponse.status(HttpStatus.BAD_REQUEST).build())
                .switchIfEmpty(ServerResponse.status(HttpStatus.NOT_FOUND).build());
    }
}
