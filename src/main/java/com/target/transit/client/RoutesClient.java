package com.target.transit.client;

import com.target.transit.dto.Route;
import com.target.transit.exception.NoTripFoundException;
import com.target.transit.exception.ServiceUnavailableException;
import com.target.transit.request.ServiceRequest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.annotation.PostConstruct;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import static java.util.Collections.singletonList;

@Service("RouteClient")
public class RoutesClient implements ServiceConsumer<List<Route>, ServiceRequest>{

    WebClient webClient;

   @PostConstruct
    public void initialize () {

       webClient = WebClient.builder()
               .baseUrl("http://svc.metrotransit.org")
               .defaultHeader(HttpHeaders.CONTENT_TYPE, "application/json")
               .defaultHeader(HttpHeaders.USER_AGENT, "Spring 5 WebClient")
               .build();
   }

    public Mono<List<Route>> consume(ServiceRequest request) {
        return webClient.get()
                .uri(uriBuilder -> {
                    uriBuilder.path("/NexTrip/Routes");
                    MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
                    map.put("format", singletonList("json"));
                    uriBuilder.queryParams(map);
                    return uriBuilder.build();
                })
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError, clientResponse -> {
                   return Mono.error(new ServiceUnavailableException("Service unavailable"));
                })
                .bodyToFlux(Route.class)
                .collectList();
    }

}
