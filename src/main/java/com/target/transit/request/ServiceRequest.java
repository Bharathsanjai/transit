package com.target.transit.request;

import lombok.*;

@Data
@Setter @Getter @NoArgsConstructor @AllArgsConstructor
public class ServiceRequest {

    public String route;
    public int direction;
    public String stop;

}
