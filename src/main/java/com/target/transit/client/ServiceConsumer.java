package com.target.transit.client;

import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
public interface ServiceConsumer<T, R> {

    public Mono<T> consume(R request);

}
