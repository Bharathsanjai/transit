package com.target.transit.client;

import com.target.transit.dto.Stop;
import com.target.transit.dto.Trip;
import com.target.transit.exception.NoTripFoundException;
import com.target.transit.request.ServiceRequest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import javax.annotation.PostConstruct;
import java.util.List;

import static java.util.Collections.singletonList;

@Service("TripClient")
public class TripClient implements ServiceConsumer<List<Trip>, ServiceRequest>{

    WebClient webClient;

   @PostConstruct
    public void initialize () {

       webClient = WebClient.builder()
               .baseUrl("http://svc.metrotransit.org")
               .defaultHeader(HttpHeaders.CONTENT_TYPE, "application/json")
               .defaultHeader(HttpHeaders.USER_AGENT, "Spring 5 WebClient")
               .build();
   }

    @Override
    public Mono<List<Trip>> consume(ServiceRequest request) {
        return webClient.get()
                .uri(uriBuilder -> {
                    uriBuilder.path("/NexTrip/" + request.route + "/" + request.direction + "/" + request.stop);
                    MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
                    map.put("format", singletonList("json"));
                    uriBuilder.queryParams(map);
                    return uriBuilder.build();
                })
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError, clientResponse -> {
                     return Mono.just(new NoTripFoundException("Invalid request "));
                })
                .onStatus(HttpStatus::is5xxServerError, clientResponse -> {
                    return Mono.just(new NoTripFoundException("Server is currently unable to process your request"));
                })
                .bodyToFlux(Trip.class)
                .collectList();
    }


}
